part of 'pages.dart';

class RegisterSuccessPage extends StatelessWidget {
  const RegisterSuccessPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const SizedBox(height: 100,),
              Center(
                child: Image.asset(
                  "assets/checklist-rise.png",
                  width: 200,
                ),
              ),
              const Text(
                "Register Success",
                style: TextStyle(
                    color: Colors.red, fontWeight: FontWeight.bold, fontSize: 40),
              ),
              const SizedBox(height: 12),
              const Text(
                "You have successfully registered an account at\nRoketin Attendance. Please check your email,\n you need to confirm your account by clicking\n the link we have sent you",
                textAlign: TextAlign.center,
              ),
              Container(
                margin: const EdgeInsets.only(
                  left: 24,
                  right: 24,
                  top: 100,
                  bottom: 12,
                ),
                width: double.infinity,
                height: 50,
                child: ElevatedButton(
                  onPressed: () {
                    Get.to(()=> HomePage());
                  },
                  style: ElevatedButton.styleFrom(
                    primary: Colors.red,
                  ),
                  child: const Text("Okay"),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
