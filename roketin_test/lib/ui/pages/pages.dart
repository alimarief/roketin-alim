import 'package:email_validator/email_validator.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/route_manager.dart';

import '../../blocs/bloc/auth_bloc.dart';

part 'login_page.dart';
part 'register_page.dart';
part 'register_success_page.dart';
part 'home_page.dart';
